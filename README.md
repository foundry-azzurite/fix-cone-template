# Rationale

See https://gitlab.com/foundrynet/foundryvtt/issues/2226

Once this issue is fixed, this module is unnecessary.

# Install

Install the module with the module.json: [`https://foundry-module.azzurite.tv/fix-cone-template/module.json`](https://foundry-module.azzurite.tv/fix-cone-template/module.json)

Or manually: [fix-cone-template.zip](https://gitlab.com/foundry-azzurite/fix-cone-template/-/jobs/artifacts/master/raw/dist/fix-cone-template.zip?job=build)

# Contributing

All contributions welcome. Keep the formatting the same.
