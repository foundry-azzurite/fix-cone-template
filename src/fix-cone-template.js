Hooks.once('init', () => {
	const old = MeasuredTemplate.prototype._getConeShape;
	MeasuredTemplate.prototype._getConeShape = function(direction, angle, distance) {
		const isFlat = game.settings.get(`core`, `coneTemplateType`) === `flat`;
		const fixedDistance = isFlat ? distance / Math.cos(toRadians(angle/2)) : distance;
		return old.call(this, direction, angle, fixedDistance);
	};
});
